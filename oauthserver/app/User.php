<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    public $primaryKey = 'm_Idx';

    protected $table = 'drmember';

    public $timestamps = false;

//    public function newQuery()
//    {
//        $query = parent::newQuery();
//        $query->whereIn('m_level', [1, 9]);
//        return $query;
//    }

    public function isLoginable() {
        return in_array($this->m_level, [1, 3, 5, 9]);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $visible = [
        'm_uid', 'm_name', 'm_email',
    ];

    public function getAuthPassword() {
        return $this->m_pwd;
    }

    public function getName() {
	    return mb_convert_encoding($this->m_name, "UTF-8", "EUC-KR, ASCII");
    }

    public function getUid() {
        return $this->m_uid;
    }
}
