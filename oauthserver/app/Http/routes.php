<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

Route::auth();

// ↓ Lilly Authorize Source Start ↓ //

// GET oauth.docple.com/login
Route::get('/login', function() {
    // return with view
    // template file is placed in resources/views/auth/login.blade.php
    return view('auth.login');
});

Route::get('/user_agreement', function(Request $request) {
    $validator = Validator::make($request->all(), [
        'redirect_uri' => 'required|url',
    ]);

    if ($validator->fails()) {
        return abort(404);
    }

    return view('user_agreement', [
        'redirect_uri' => $request->get('redirect_uri'),
    ]);
});

Route::post('user_agreement', function(Request $request) {
    $validator = Validator::make($request->all(), [
        'redirect_uri' => 'required|url',
        'agreement' => 'required',
    ], [
        'agreement.required' => '개인정보 제3자 정보제공에 관한 동의가 필요합니다.',
    ]);

    if ($validator->fails()) {
        return redirect()->back()->withErrors($validator);
    }

    $exists = \DB::table('user_agreement')->where('user_id', Auth::user()->getKey())->exists();

    if (!$exists) {
        \DB::table('user_agreement')->insert([
            'user_id' => Auth::user()->getKey(),
            'created_at' => date('Y-m-d H:i:s'),
        ]);  
    }

    return Redirect::to($request->get('redirect_uri'));
});

// GET oauth.docple.com/api/me
Route::get('/api/me', ['middleware' => ['oauth', 'cors'], function() {
    // get user id
    $id = Authorizer::getResourceOwnerId();

    // find user in database
    // if it's not found returned 404 error
    $user = \App\User::findOrFail($id);

    $info = \DB::table('drmem_info')->where('m_uid', $user->m_uid)->first();

//    $out['id'] = 'Docple_' . $user->m_uid;
    $out['id'] = $user->m_uid;
    // there is encoding issue with korean characters when trying to convert array to json
    // encode name to UTF-8
    $out['name'] = mb_convert_encoding($user->m_name, "UTF-8", "EUC-KR, ASCII");
    $out['hospital'] = isset($info->m_hospital) && !empty($info->m_hospital) ? mb_convert_encoding($info->m_hospital, "UTF-8", "EUC-KR, ASCII") : '';
    $out['medicaitems'] = isset($info->m_medicaitems) && !empty($info->m_medicaitems) ? mb_convert_encoding($info->m_medicaitems, "UTF-8", "EUC-KR, ASCII") : '';
    $out['useyn'] = 'Y' == $user->m_useYN ? 'Y' : 'N';

    return response()->json($out);
}]);

// ↑ Lilly Authorize Source End ↑ //

// ↓ Default Authorize Source Start ↓ //

// GET oauth.docple.com/api/default
Route::get('/api/default', ['middleware' => ['oauth', 'cors'], function() {
    // get user id
    $id = Authorizer::getResourceOwnerId();

    // find user in database
    // if it's not found returned 404 error
    $user = \App\User::findOrFail($id);

    $info = \DB::table('drmem_info')->where('m_uid', $user->m_uid)->first();

//    $out['id'] = 'Docple_' . $user->m_uid;
    $out['id'] = $user->m_uid;
    // there is encoding issue with korean characters when trying to convert array to json
    // encode name to UTF-8
    $out['name'] = mb_convert_encoding($user->m_name, "UTF-8", "EUC-KR, ASCII");
    $out['email'] = isset($user->m_email) && !empty($user->m_email) ? $user->m_email : '';
    $out['useyn'] = 'Y' == $user->m_useYN ? 'Y' : 'N';

    return response()->json($out);
}]);

Route::post('/logout', 'Auth\AuthController@doLogout');

Route::get('/', function(){
    return view('home');
});

Route::post('oauth/access_token', ['middleware' => 'cors', function() {
    return response()->json(Authorizer::issueAccessToken());
}]);

Route::get('/agreement', function(Request $request) {
    $validator = Validator::make($request->all(), [
        'redirect_uri' => 'required|url',
    ]);

    if ($validator->fails()) {
        return abort(404);
    }

    return view('user_agreement', [
        'redirect_uri' => $request->get('redirect_uri'),
    ]);
});

Route::post('agreement', function(Request $request) {
    $validator = Validator::make($request->all(), [
        'redirect_uri' => 'required|url',
        'agreement' => 'required',
    ], [
        'agreement.required' => '개인정보 제3자 정보제공에 관한 동의가 필요합니다.',
    ]);

    if ($validator->fails()) {
        return redirect()->back()->withErrors($validator);
    }

    $exists = \DB::table('oauth_agreement')->where('user_id', Auth::user()->getKey())->exists();

    if (!$exists) {
        \DB::table('oauth_agreement')->insert([
            'user_id' => Auth::user()->getKey(),,
			'client_id' => $request->get('client_id'),
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }

    return Redirect::to($request->get('redirect_uri'));
});

Route::get('oauth/authorize', ['as' => 'oauth.authorize.get', 'middleware' => ['check-authorization-params', 'auth'], function() {
    $authParams = Authorizer::getAuthCodeRequestParams();

    // if user already logged in
    if (Auth::check()) {
        $user = Auth::user();

        $authParams['user_id'] = $user->getKey();

        $redirectUri = Authorizer::issueAuthCode('user', $authParams['user_id'], $authParams);

        return Redirect::to($redirectUri);
    }

    $formParams = array_except($authParams,'client');

    $formParams['client_id'] = $authParams['client']->getId();

    $formParams['scope'] = implode(config('oauth2.scope_delimiter'), array_map(function ($scope) {
        return $scope->getId();
    }, $authParams['scopes']));

    return View::make('auth.authorization-form', ['params' => $formParams, 'client' => $authParams['client']]);
}]);

Route::post('oauth/authorize', ['as' => 'oauth.authorize.post', 'middleware' => ['csrf', 'check-authorization-params', 'auth'], function() {
    $params = Authorizer::getAuthCodeRequestParams();

    $user = Auth::user();

    $params['user_id'] = $user->getKey();
    $params['user_uid'] = $user->getUid();

    $redirectUri = '/';

    // If the user has allowed the client to access its data, redirect back to the client with an auth code.
    if (Request::has('continue')) {
        $redirectUri = Authorizer::issueAuthCode('user', $params['user_id'], $params);
    }

    // If the user has denied the client to access its data, redirect back to the client with an error message.
    if (Request::has('cancel')) {
        $redirectUri = Authorizer::authCodeRequestDeniedRedirectUri();
    }

    return Redirect::to($redirectUri);
}]);

// ↑ Default Authorize Source End ↑ //