<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use function parse_url;
use function stristr;

class CheckDocpleCookie
{
    private $allowedDomains = [
        'www.docple.com',
        'www1.docple.com',
        'docple.com',
    ];

    public function handle($request, Closure $next) {
        $referrer = $request->headers->get('referer');

        if (!empty($referrer)) {
            $parsedUrl = parse_url($referrer);

            if (in_array($parsedUrl['host'], $this->allowedDomains)) {
                if (!empty($_COOKIE['docple'])) {
                    $docpleCookie = $_COOKIE['docple'];

                    if (!empty($docpleCookie)) {
                        parse_str($docpleCookie, $output);

                        if (!empty($output['id'])) {
                            $user = User::where('m_uid', $output['id'])->first();

                            if ($user) {
                                Auth::login($user);
                            }
                        }
                    }
                }
            }
        }

        return $next($request);
    }
}