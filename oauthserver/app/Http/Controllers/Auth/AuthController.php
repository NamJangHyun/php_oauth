<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    const DOCPLE_URL = 'http://www1.docple.com';
    const DOCPLE_LOGIN_FILE = '/77_member/member_oauth_log_ex.aspx';

    use AuthenticatesUsers {
        login as protected traitLogin;
    }

    use ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        $this->middleware('guest', ['except' => ['logout', 'getLogout']]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function loginUsername()
    {
        return 'm_uid';
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        $post['user_id'] = $request->get('m_uid');
        $post['user_pwd'] = $request->get('password');

        $result = $this->sendLoginRequest($post);

        $json = json_decode($result['body']);


        if (!$json) {
            return $this->sendFailedLoginResponse($request);
        }

        if ($json->STATE == 'OK') {
            $user = User::where('m_uid', $request->get('m_uid'))->first();

            if (!$user) {
                return $this->sendFailedLoginResponse($request);
            }

            if (!$user->isLoginable()) {
                return redirect()->back()
                    ->withInput($request->only('m_uid', 'remember'))
                    ->withErrors([
                        'msg' => '해당 서비스는 의사 회원만 사용이 가능 합니다.',
                    ]);
            }

            if (!empty($result['cookies'])) {
                foreach ($result['cookies'] as $key => $value) {
                    setrawcookie($key, $value, time()+60*60*24*30, '/', '.docple.com');
                }
            }

            return $this->traitLogin($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    public function doLogout(Request $request) {
        try {
            $accessToken = Authorizer::getAccessToken();

            if ($accessToken) {
                $accessToken->expire();
            }
        } catch (\Exception $e) {
            // ignore
        }

//        setcookie('ASP.NET_SessionId', '', time(), '/');
//        setcookie('docple', '', time(), '/');
//        setcookie('laravel_session', '',time(), '/');

        Auth::guard($this->getGuard())->logout();

        if ($request->isXmlHttpRequest()) {
            return response()->json(['status' => 'success']);
        }

        return redirect()->back();
    }

    private function sendLoginRequest($parameters)
    {
        $fields_string = '';

        foreach ($parameters as $key => $value) {
            $fields_string .= $key . '=' . urlencode($value) . '&';
        }
        rtrim($fields_string, '&');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, self::DOCPLE_URL . self::DOCPLE_LOGIN_FILE);
        curl_setopt($ch, CURLOPT_POST, count($parameters));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');

        $result = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($result, 0, $header_size);
        $body = substr($result, $header_size);

        curl_close($ch);

        $out['body'] = mb_convert_encoding($body, "UTF-8", "EUC-KR, ASCII");
        $out['cookies'] = $this->parseCookiesFromHeader($header);

        return $out;
    }

    private function parseCookiesFromHeader($header) {
        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);

        $cookies = array();

        foreach($matches[1] as $item) {
            list($key, $value) = explode('=', $item, 2);
            $cookies[$key] = $value;
        }

        return $cookies;
    }
}
