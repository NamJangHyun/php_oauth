@if ($errors->has('msg'))
    <div style="clear:both;padding-top:15px; padding-bottom: 15px;">
        <div style="background-color:#f2dede; padding: 15px 0;">
            <ul style="color: #a94442; padding: 0 5px; margin: 0; list-style-type: none; text-align: center;">
                <li>{{ $errors->first('msg') }}</li>
            </ul>
        </div>
    </div>
@endif

<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('m_uid') ? ' has-error' : '' }}">
        <div class="col-md-6 col-sm-6 col-xs-6">
            <span>닥플로그인</span>
        </div>
        {{--<div class="col-md-6 col-sm-6 col-xs-6 text-right">--}}
            {{--<label>--}}
                {{--<input type="checkbox" name="remember"> 로그인 상태 유지--}}
            {{--</label>--}}
        {{--</div>--}}
        <div class="col-md-12">
            <input id="m_uid" type="text" class="form-control custom-input" name="m_uid" value="{{ old('m_uid') }}">

            @if ($errors->has('m_uid'))
                <span class="help-block">
                    <strong>{{ $errors->first('m_uid') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <div class="col-md-12">
            <input id="password" type="password" class="form-control custom-input" name="password">

            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary btn-login" id="button-login">로그인</button>
        </div>
    </div>
</form>

<div class="col-md-6 col-sm-6 col-xs-6">
    <a href="http://docple.com/77_member/joinPage_d00.aspx">회원가입</a>
</div>
<div class="col-md-6 col-sm-6 col-xs-6 text-right">
    <a href="http://docple.com/00_include/pop/pop_id_pwdfind_d01.aspx">ID/PW찾기</a>
</div>

<script>
//    $(function() {
//        $('#button-login').on('click', function(e) {
//            $.post('docple.com/77_member/member_oauth_log_ex.aspx', {
//                'user_id': 'docpleit',
//                'user_pwd': '#docple12345'
//            }, function(data) {
//                console.log(data);
//            });
//
//            return false;
//        });
//    });
</script>