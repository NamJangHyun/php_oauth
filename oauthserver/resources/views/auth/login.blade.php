@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1" id="login-box">
                @include('auth.auth-header')

                @include('auth.login-form')
            </div>
        </div>
    </div>
@endsection
