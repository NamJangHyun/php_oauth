<form method="post" action="{{route('oauth.authorize.post', $params)}}">
    {{ csrf_field() }}

    <input type="hidden" name="client_id" value="{{$params['client_id']}}">
    <input type="hidden" name="redirect_uri" value="{{$params['redirect_uri']}}">
    <input type="hidden" name="response_type" value="{{$params['response_type']}}">
    <input type="hidden" name="state" value="{{$params['state']}}">
    <input type="hidden" name="scope" value="{{$params['scope']}}">

    <p class="login-text">
        {{ $client->getName() }} will receive:<br/>
        your public profile.
    </p>

    <div class="col-md-12 bottom-buffer">
        <button type="submit" class="btn btn-primary btn-auth" name="continue" value="1">Continue as {{ \Auth::user()->getName() }}</button>
    </div>

    <div class="col-md-12">
        <button type="submit" class="btn btn-default btn-auth" name="cancel" value="1">Cancel</button>
    </div>
</form>