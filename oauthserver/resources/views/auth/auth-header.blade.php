<div id="lillyon-logo">
    <img src="../img/lillyon-logo.png"/>
</div>

<p class="login-text">
    닥플 계정으로 로그인 하시면<br/>
    한국릴리가 제공하는<br/>
    모든 정보를 확인하실 수 있습니다!
</p>

<div id="docple-logo">
    <img src="../img/docple-logo.png"/>
</div>