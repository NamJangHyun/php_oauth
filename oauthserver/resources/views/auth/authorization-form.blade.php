@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1" id="login-box">
                <div id="lillyon-logo">
                    <img src="../img/lillyon-logo.png"/>
                </div>

                <p class="login-text">
                    닥플 계정으로 로그인 하시면<br/>
                    한국릴리가 제공하는<br/>
                    모든 정보를 확인하실 수 있습니다!
                </p>

                <div id="docple-logo">
                    <img src="../img/docple-logo.png"/>
                </div>

                @include('auth.auth-form')
            </div>
        </div>
    </div>
@endsection
