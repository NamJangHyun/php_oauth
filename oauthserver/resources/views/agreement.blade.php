<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!--meta http-equiv="content-type" content="text/html; charset=euc-kr" /-->
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>Docple</title>
    <style>
        html, body {
            margin: 0 0 0 0;
            padding: 0 0 0 0;
            font-size: 12px;
            width: 100%;
            height: 100%;
        }

        div.tcenter {
            text-align: center
        }
    </style>
</head>
<body>
<div>
    <div style="width:100%;margin: 40px auto;">
        <div class="tcenter">
            <!--릴리로그--->
            <img src="http://docple.com/images/index/lilly/Lilly_LOGO.png" width="100px">
        </div>
        <!--라인--->
        <div style="padding-top:5px;">
            <div style="height:4px; background-image : url('http://docple.com/images/index/lilly/Long_Bar.jpg')">
            </div>
        </div>
        <div style="width:800px;margin: 40px auto;">
            <div>
                <div class="tcenter">
                    최신 의학 정보 포탈 서비스, <span style="font-weight:bold;">릴리온(LillyON)</span>에 오신 것을 환영합니다!
                </div>
                <div style="padding-top:10px;"></div>
                <!--동영상--->
                <div style="width:800px;background-color:red;" class="tcenter">
                    <div style="float:left;width:120px">&nbsp;</div>
                    <div style="float:left;width:560px;background-color:blue;">
                        <script src="https://www.kaltura.com/p/1759891/sp/175989100/embedIframeJs/uiconf_id/29415272/partner_id/1759891?autoembed=true&entry_id=1_kv2tbv0l&playerId=kaltura_player&width=560&height=395"></script>
                    </div>
                    <div style="float:left;width:120px">&nbsp;</div>
                </div>
            </div>

            @if ($errors->any())
                <div style="clear:both;padding-top:15px;width:800px;">
                    <div style="width:600px;background-color:#f2dede;margin:0 100px 0 100px; padding: 15px 0;">
                        <ul style="color: #a94442;">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif

            <!--회색 설명--->
            <div style="clear:both;padding-top:15px;width:800px;">
                <div style="width:600px;background-color:#eeeeee;margin:0 100px 0 100px;">
                    <div style="padding:15px">
                        릴리온 (LillyON)은 의사 선생님들에게 최신 임상 연구 및 학술 정보를 통합적으로 제공하는 서비스로 실시간 온라인 미팅 참여 및
                        진행된 온라인 미팅 다시보기, 1:1 영어 미팅 및 최신 연구 논문 찾기 등의 서비스를 제공합니다.<br/>
                        시간과 장소에 구애 없이 선생님들이 원하실 때 최신 의료 지견을 확인하실 수 있습니다.<br/>
                        서비스 이용과정에서 IP Address, 쿠키, 방문 일시, 서비스 이용 기록이 자동으로 생성되어 수집될 수 있습니다<br/>
                    </div>
                </div>
            </div>

            <!--삼자동의서 설명--->
            <div style="padding-top:15px;width:800px;">
                <div style="width:600px;margin:0 100px 0 100px;border:1px solid #dedede;">
                    <div style="padding:5px 5px 5px 5px">
                        <div style="width:600px;text-align:center;font-weight:bold;padding-bottom:5px;">*개인정보 제3자 제공에 관한
                            동의서*
                        </div>
                        <div>닥플주식회사는 한국릴리의 최신 의학 정보 포탈 서비스 릴리온(LillyON)의 원활한 서비스 제공을 위하여 아래와 같은 귀하의 개인 정보를 제 3자에게 제공하오니,
                            동의하실 경우, 하단 박스에 체크해 주시기를 바랍니다.
                        </div>
                        <div style="padding:10px">
                            - 제공처: 한국릴리(유)<br/>
                            - 필수정보: 이름, 근무처명, 전공과목<br/>
                            - 보유 및 이용기간 : 제휴 서비스 종료 시까지<br/>
                        </div>
                    </div>
                </div>
            </div>

            <form method="post" action="/user_agreement">
                <input type="hidden" name="redirect_uri" value="{{ $redirect_uri }}">
                <!--체크박스--->
                <div style="padding-top:15px;width:800px;">
                    <div style="width:600px;margin:0 100px 0 100px;">
                        <div>
                            <input type=checkbox name="agreement" id="ch1" value="tete"><label for=ch1 style="cursor:pointer">상기
                                정보 제공에 동의합니다.</label>
                        </div>
                    </div>
                </div>
                <div style="margin:10px 0 10px 0;"></div>
                <!--버튼--->
                <div class=tcenter>
                    <input type="image" src='http://docple.com/images/index/lilly/Confitm_Button.jpg'
                           style="width:150px;cursor:pointer">
                </div>
            </form>

        </div>
    </div>
</div>
</body>
</html>
